import firebase from "firebase/app";
import 'firebase/firestore';
import 'firebase/auth';

var firebaseConfig = {
    apiKey: "AIzaSyA1pSy6F65U-Qb8T1Osb_QJcmmcCJNPIHQ",
    authDomain: "net-ninja-mario-paln.firebaseapp.com",
    databaseURL: "https://net-ninja-mario-paln.firebaseio.com",
    projectId: "net-ninja-mario-paln",
    storageBucket: "net-ninja-mario-paln.appspot.com",
    messagingSenderId: "934498746508",
    appId: "1:934498746508:web:5ee2217d679de05aa27ba9",
    measurementId: "G-8B4KBZR2ZN"
};
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({ })

export default firebase;