import React, { Component } from "react";
import {Redirect} from "react-router-dom";
import { connect } from 'react-redux';
import {signUp} from "../../store/action/authAction";


class Signup extends Component {
    state = {
        email: '',
        password: '',
        firstName: '',
        lastName: ''
    }
    handelChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        // console.log(this.state);
        this.props.signUp(this.state)
    }
    render() {
        const {auth, authError} = this.props
        if (auth.uid) return <Redirect to='/'/>
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit} className="white">
                    <h5 className="grey-text text-darken-3">Join Us</h5>
                    <div className="input-field">
                        <label htmlFor="email">Email</label>
                        <input type="email" id='email' onChange={this.handelChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="password">Password</label>
                        <input type="password" id='password' onChange={this.handelChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="firstName">First Name</label>
                        <input type="text" id='firstName' onChange={this.handelChange} required={true}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="lastName">last Name</label>
                        <input type="text" id='lastName' onChange={this.handelChange} required={true}/>
                    </div>
                    <div className="input field">
                        <button className="btn pink lighten-1 z-depth-0">Join</button>
                        <div className="red-text center">
                            { authError ? <p>{ authError }</p>: null}
                        </div>
                    </div>
                </form>
                <h3 className='center pink-text'>"Join and have fun!!!<span role='img'>🎀</span>"</h3>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth,
        authError: state.auth.authError
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        signUp: (newUser) => dispatch(signUp(newUser))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Signup);