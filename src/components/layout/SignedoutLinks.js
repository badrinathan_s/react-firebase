import React from "react";
import { NavLink } from "react-router-dom";

const SignedoutLinks = () =>{
    return (
        <ul className="right">
            <li><NavLink to='/signup'>Join Us</NavLink></li>
            <li><NavLink to='/signin'>Pass In</NavLink></li>
        </ul>
    )
}

export default SignedoutLinks;